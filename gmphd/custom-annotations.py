import numpy as np
f= open("custom-annot.idl","w+")

for i in range(1,83):
    diff = 5
    box1Start = [0,430,50,480]
    box2Start = [590,430,640,480]
    boxes = np.array(np.zeros([4,2]), dtype=np.float64)
    boxes[0,0] = box1Start[0] + i*diff
    boxes[1,0] = box1Start[1] - i*diff
    boxes[2,0] = box1Start[2] + i*diff
    boxes[3,0] = box1Start[3] - i*diff
    boxes[0,1] = box2Start[0] - i*diff
    boxes[1,1] = box2Start[1] - i*diff
    boxes[2,1] = box2Start[2] - i*diff
    boxes[3,1] = box2Start[3] - i*diff
    
    f.write("\"left/image" + str(i) + ".png\": (" + str(boxes[0,0]) + ", " + str(boxes[1,0]) + ", " + str(boxes[2,0]) + ", " + str(boxes[3,0]) + "), (" + str(boxes[0,1]) + ", " + str(boxes[1,1]) + ", "+str(boxes[2,1]) + ", "+str(boxes[3,1]) + ")\n")
    # format(i, boxes[0,0], boxes[0,1], boxes[1,0], boxes[1,1])
    
f.close()