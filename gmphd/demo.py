from gmm.gen_model import gen_model
from truth.gen_truth import gen_truth
from meas.gen_meas import gen_meas
from phd.run_filter import run_filter
from result.plot_results import plot_results
from result.video import make_video

dataset = 'bahnhof'  # bahnhof or lp or custom

model = gen_model()
truth = gen_truth(model, dataset)
# meas = gen_meas(model, truth, 'noiseless')
# est = run_filter(model, meas)
# plot_results(dataset, model, truth, est)

make_video(dataset)
