import numpy as np
import numpy.matlib


def gaus_merge(w=None, x=None, P=None, threshold=None):

    L = len(w)
    x_dim = x.shape[0]
    I = [i for i in range(L)]
    el = 0

    w_new = np.zeros((0, 1))
    x_new = np.zeros((x_dim, 1))
    P_new = np.zeros((x_dim, x_dim))

    if all(w == 0):
        w_new = []
        x_new = []
        P_new = []

        return w_new, x_new, P_new

    w_new = np.zeros((0, 1))
    x_new = np.zeros((x_dim, 0))
    P_new = np.zeros((0, x_dim, x_dim))

    while I != []:

        j = np.argmax(w, axis=0)
        j = j[0]
        Ij = []
        iPt = np.linalg.inv(P[j])

        for i in I:
            val = np.dot(np.dot((x[:, i] - x[:, j]).T, iPt),
                         (x[:, i] - x[:, j]))

            if val <= threshold:
                Ij.append(i)
        # === w
        w_t = np.array([0]).reshape(1, 1)
        w_new = np.insert(w_new, el, w_t, axis=0)
        w_new[el] = np.sum(w[Ij])

        # === x
        x_t = np.zeros((x_dim, 1))
        x_new = np.insert(x_new, el, x_t.T, axis=1)
        x_new[:, el] = wsumvec(w[Ij], x[:, Ij], x_dim).T

        # === P
        P_t = np.zeros((x_dim, x_dim))
        P_new = np.insert(P_new, el, P_t, axis=0)
        P_new[el] = wsummat(w[Ij], P[Ij], x_dim)

        x_new[:, el] = x_new[:, el] / w_new[el]
        P_new[el] = P_new[el] / w_new[el]

        I = np.setdiff1d(I, Ij)

        w[Ij] = -1
        el += 1

    return w_new, x_new, P_new


def wsumvec(w=None, vecstack=None, xdim=None):

    wmat = np.tile(w.T, (xdim, 1))
    out = np.sum(np.multiply(wmat, vecstack), 1)
    out = out.reshape(out.size, -1)

    return out


def wsummat(w=None, matstack=None, xdim=None):

    wmat = np.ones((w.size, xdim, xdim))

    for i, p in enumerate(w):
        wmat[i] = wmat[i] * p

    out = np.sum(np.multiply(wmat, matstack), 0)

    return out
