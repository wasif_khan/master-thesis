

def gaus_prune(w=None, x=None, P=None, elim_threshold=None):

    def get_indexes(x, xs): return [i for (
        y, i) in zip(xs, range(len(xs))) if x < y]

    idx = get_indexes(elim_threshold, w)

    w_new = w[idx]
    x_new = x[:, idx]
    P_new = P[idx]

    return w_new, x_new, P_new
