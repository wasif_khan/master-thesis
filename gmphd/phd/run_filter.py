import numpy as np
import numpy.matlib
from scipy.stats.distributions import chi2
import sys
from phd.kalman_predict_multiple import kalman_predict_multiple
from phd.gate_meas_gms import gate_meas_gms
from phd.kalman_update_multiple import kalman_update_multiple
from phd.gaus_prune import gaus_prune
from phd.gaus_merge import gaus_merge
from phd.gaus_cap import gaus_cap


def run_filter(model=None, meas=None):
    '''
    This is the Python code for the GM-PHD filter by Akolade Tinubu,
    proposed in (assuming no target spawning) B.-N. Vo, and W. K. Ma, "The Gaussian mixture Probability Hypothesis Density Filter,"
    IEEE Trans Signal Processing, Vol. 54, No. 11, pp. 4091-4104, 2006.
    http://ba-ngu.vo-au.com/vo/VM_GMPHD_SP06.pdf
    '''

    print('Performing Gaussian Mixture Probability Hypothesis Filtering')
    print('Please wait...')

    # === Setup

    est = {}
    filter_ = {}

    # output variables
    est['estimate_boxes'] = [[] for i in range(meas['length'])]
    est['repeated_number_of_boxes'] = np.zeros(meas['length'], dtype=int)
    est['L'] = [[] for i in range(meas['length'])]

    # filter parameters

    # limit on number of Gaussians
    filter_['L_max'] = 100

    # pruning threshold
    filter_['elim_threshold'] = 1e-05

    # merging threshold
    filter_['merge_threshold'] = 4

    # gate size in percentage
    filter_['P_G'] = 0.999

    # inv chi^2 dn gamma value
    filter_['gamma'] = chi2.ppf(filter_['P_G'], model['z_dim'])

    # gating on or off 1/0
    filter_['gate_flag'] = 1

    # 'disp' or 'silence' for on the fly output
    filter_['run_flag'] = 'silence'

    est['filter'] = filter_

    # # === Filtering

    # initial prior
    w_update = sys.float_info.epsilon
    m_update = np.array(
        [210, 0, 210, 0, 235, 0, 270, 0]
    ).reshape(model['x_dim'], -1)
    P_update = np.diag(np.array([1, 1, 1, 1, 1, 1, 1, 1]).T) ** 2
    L_update = 1

    # recursive filtering
    for k in range(meas['length']):

        # ===---prediction
        # surviving components
        m_predict, P_predict = kalman_predict_multiple(
            model, m_update, P_update)

        # surviving weights
        w_predict = np.array([np.dot(model['P_S'], w_update)]).reshape(-1, 1)

        # append birth components
        m_predict = np.concatenate((model['m_birth'], m_predict), axis=1)

        P_predict = np.insert(
            model['P_birth'], model['P_birth'].shape[0], P_predict, axis=0)

        # append birth weights
        w_predict = np.concatenate(
            (model['w_birth'], w_predict), axis=0)

        # number of predicted components
        L_predict = model['L_birth'] + L_update

        # ===---gating
        if filter_['gate_flag']:
            meas['objects'][k] = gate_meas_gms(
                meas['objects'][k], filter_['gamma'], model, m_predict, P_predict)

        # ===---update
        # number of measurements
        try:
            m = meas['objects'][k].shape[1]

            # missed detection term
            w_update = np.dot(model['Q_D'], w_predict)
            m_update = m_predict
            P_update = P_predict

            if m != 0:
                # m detection terms
                qz_temp, m_temp, P_temp = kalman_update_multiple(
                    meas['objects'][k], model, m_predict, P_predict)

                for ell in range(m):
                    w_temp = np.multiply(
                        np.dot(model['P_D'], np.ravel(w_predict)), qz_temp[:, ell])

                    w_temp = w_temp / (
                        np.dot(model['lambda_c'],
                               model['pdf_c']) + np.sum(w_temp)
                    )

                    w_update = np.concatenate(
                        (w_update, w_temp.reshape(w_temp.size, -1)))

                    m_update = np.concatenate(
                        (m_update, m_temp[:, :, ell].T), axis=1)

                    P_update = np.insert(
                        P_update, P_update.shape[0], P_temp, axis=0)

        except AttributeError as _:
            pass

        # ===---mixture management

        # prune
        L_posterior = len(w_update)

        w_update, m_update, P_update = gaus_prune(
            w_update, m_update, P_update, filter_['elim_threshold'])
        L_prune = len(w_update)

        # merge
        w_update, m_update, P_update = gaus_merge(
            w_update, m_update, P_update, filter_['merge_threshold'])
        L_merge = len(w_update)

        # cap
        w_update, m_update, P_update = gaus_cap(
            w_update, m_update, P_update, filter_['L_max'])
        L_cap = len(w_update)

        L_update = L_cap

        # ===---state extraction

        def get_indexes(x, xs): return [i for (
            y, i) in zip(xs, range(len(xs))) if x < y]

        idx = get_indexes(0.5, w_update)

        for j in range(len(idx)):
            repeat_num_targets = np.round(w_update[idx[j]])

            repeat_num_targets = int(repeat_num_targets)

            est['estimate_boxes'][k].append(
                np.tile(m_update[:, idx[j]], (1, repeat_num_targets)))

            est['repeated_number_of_boxes'][k] = est['repeated_number_of_boxes'][k] + repeat_num_targets
            est['L'][k] = []

        # ===---display diagnostics
        if filter_['run_flag'] is not 'silence':
            print('time= {} #est mean= {} #est card= {} #est orig= {} #est elim= {} #est merge={}'.format(
                k, np.sum(w_update).round(4), est['repeated_number_of_boxes'][k], L_posterior, L_prune, L_merge))

    print('Process completed')
    print('')

    return est
