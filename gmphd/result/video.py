import os
from pathlib import Path
import cv2


def make_video(dataset=None):

    image_folder = Path(__file__).resolve().parent.parent.parent / \
        'datasets/images/{}/png/'.format(dataset)

    video = 'video.mp4'

    images = list(image_folder.glob('*.png'))
    images.sort(key=lambda x: os.path.getmtime(x))

    frame = cv2.imread(str(images[0]))
    height, width, layers = frame.shape

    video = cv2.VideoWriter(video, 0x7634706d, 14, (width, height))

    for image in images:
        video.write(cv2.imread(str(image)))

    cv2.destroyAllWindows()
    video.release()
