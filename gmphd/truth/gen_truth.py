import numpy as np
from pathlib import Path
from PIL import Image
import re
from ast import literal_eval as make_tuple
from meas.gen_meas import gen_meas
from phd.run_filter import run_filter
from result.plot_results import plot_results


def gen_truth(model=None, dataset=None):

    print('Loading data from dataset')
    print('Please wait...')

    base_dir = Path(__file__).resolve().parent.parent.parent
    img_dir_len = 'datasets/images/{}/left/'.format(dataset)
    img_dir = 'datasets/images/{}/'.format(dataset)
    anno = 'datasets/annotation/{}-annot.idl'.format(dataset)
    anno = base_dir.joinpath(anno)

    img_dir_len = base_dir.joinpath(img_dir_len)
    img_dir = base_dir.joinpath(img_dir)

    k = 0

    truth = {}

    # length of data/number of scans
    truth['length'] = 1

    # ground truth for states of targets
    truth['groud_truth'] = [[]]

    # frames
    truth['Images'] = [[]]

    # ground truth for number of targets
    truth['no_of_targets'] = np.zeros(truth['length'], dtype=int)

    # ground truth for labels of targets (k,i)
    truth['labels'] = [[]]

    # absolute index target identities (plotting)
    truth['track_list'] = [[]]

    # total number of appearing tracks
    truth['total_tracks'] = 0

    truth['high'] = 0

    with open(anno, 'r') as f:
        for i in f:
            # np.empty(truth['groud_truth'][0])
            truth['groud_truth'] = [[]]
            # clean annotation
            value = np.array(re.sub('[\n;"]', '', i).split(': '))

            img = np.array(Image.open(img_dir / value[0]))
            truth['Images'][0] = img

            value = np.array(make_tuple(value[1])).T
            value = np.insert(value, 1, [0], axis=0)
            value = np.insert(value, 3, [0], axis=0)
            value = np.insert(value, 5, [0], axis=0)
            value = np.insert(value, 7, [0], axis=0)

            truth['groud_truth'][0] = value
            truth['no_of_targets'][0] = value.shape[1]

            truth['Images'] = np.array(truth['Images'])
            truth['groud_truth'] = np.array(truth['groud_truth'])

            meas = gen_meas(model, truth, 'noiseless')
            est = run_filter(model, meas)
            plot_results(dataset, model, truth, est, k)

            if truth['no_of_targets'][0] > truth['high']:

                # highest number of targets on a scene
                truth['high'] = truth['no_of_targets'][0]

            k += 1


    print('Data successfully loaded')
    print('')

    return truth
