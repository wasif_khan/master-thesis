import sys
# TODO: import all modules without gmphd. in all files of gmphd
# sys.path.append("../gmphd/gmm")
# print(sys.path)
import matplotlib
matplotlib.use('Agg')
from gmphd.gmm.gen_model import gen_model
from gmphd.truth.gen_truth import gen_truth
from gmphd.meas.gen_meas import gen_meas
from gmphd.phd.run_filter import run_filter
from gmphd.result.plot_results import plot_results
from gmphd.result.video import make_video

dataset = 'bahnhof'  # bahnhof or lp or custom
def run():
    model = gen_model()
    truth = gen_truth(model, dataset)
    meas = gen_meas(model, truth, 'noiseless')
    est = run_filter(model, meas)
    plot_results(dataset, model, truth, est)

    # make_video(dataset)
