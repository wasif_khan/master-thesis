import numpy as np


def gen_model():

    print('Computing model components...')

    model = {}

    # basic parameters
    # dimension of state vector
    model['x_dim'] = 8

    # dimension of observation vector
    model['z_dim'] = 4

    # dynamical model parameters (CV model)
    # sampling period
    model['T'] = 1

    model['FT'] = 1 / 14

    # transition matrix
    x = np.array([1, model['T'], 0, 0, 0, 0, 0, 0])
    vx = np.array([0, 1, 0, 0, 0, 0, 0, 0])
    y = np.array([0, 0, 1, model['T'], 0, 0, 0, 0])
    vy = np.array([0, 0, 0, 1, 0, 0, 0, 0])
    w = np.array([0, 0, 0, 0, 1, model['T'], 0, 0])
    vw = np.array([0, 0, 0, 0, 0, 1, 0, 0])
    h = np.array([0, 0, 0, 0, 0, 0, 1, model['T']])
    vh = np.array([0, 0, 0, 0, 0, 0, 0, 1])

    model['F'] = np.array([x, vx, y, vy, w, vw, h, vh])

    b1 = np.array([(model['T'] ** 2) / 2, model['T'], 0, 0,
                   0, 0, 0, 0]).reshape(model['x_dim'], 1)
    b2 = np.array([0, 0, (model['T'] ** 2) / 2, model['T'],
                   0, 0, 0, 0]).reshape(model['x_dim'], 1)
    b3 = np.array([0, 0,  0, 0, (model['T'] ** 2) / 2,
                   model['T'], 0, 0]).reshape(model['x_dim'], 1)
    b4 = np.array([0, 0, 0, 0, 0, 0, (model['T'] ** 2) / 2,
                   model['T']]).reshape(model['x_dim'], 1)

    model['sigma_v'] = 5
    model['B'] = np.concatenate((b1, b2, b3, b4), axis=1)

    # process noise covariance
    model['Q'] = np.dot(
        np.dot((model['sigma_v'] ** 2), model['B']), model['B'].T
    )

    # survival / death parameters
    model['P_S'] = 0.99
    model['Q_S'] = 1 - model['P_S']

    # birth parameters (Poisson birth model, multiple Gaussian components)
    # no. of Gaussian birth terms
    model['L_birth'] = 8

    # weights of Gaussian birth terms (per scan) [sum gives average rate of target birth per scan]
    model['w_birth'] = np.zeros((model['L_birth'], 1))

    # means of Gaussian birth terms
    model['m_birth'] = np.zeros((model['x_dim'], model['L_birth']))

    # std of Gaussian birth terms
    model['B_birth'] = np.zeros(
        (model['x_dim'], model['x_dim'], model['L_birth'])
    )

    # cov of Gaussian birth terms
    model['P_birth'] = np.zeros(
        (model['x_dim'], model['x_dim'], model['L_birth'])
    )

    # birth term 1
    model['w_birth'][0] = 3 / 100
    model['m_birth'][:, 0] = np.array([212, 0, 204, 0, 232, 0, 261, 0])
    model['B_birth'][0] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][0] = np.dot(
        model['B_birth'][0], model['B_birth'][0].T)

    # birth term 2
    model['w_birth'][1] = 3 / 100
    model['m_birth'][:, 1] = np.array([223, 0, 181, 0, 259, 0, 285, 0])
    model['B_birth'][1] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][1] = np.dot(
        model['B_birth'][1], model['B_birth'][1].T)

    # birth term 3
    model['w_birth'][2] = 3 / 100
    model['m_birth'][:, 2] = np.array([293, 0, 151, 0, 354, 0,  325, 0])
    model['B_birth'][2] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][2] = np.dot(
        model['B_birth'][2], model['B_birth'][2].T)

    # birth term 4
    model['w_birth'][3] = 3 / 100
    model['m_birth'][:, 3] = np.array([452, 0, 208, 0, 479, 0, 276, 0])
    model['B_birth'][3] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][3] = np.dot(
        model['B_birth'][3], model['B_birth'][3].T)

    # birth term 5
    model['w_birth'][4] = 3 / 100
    model['m_birth'][:, 4] = np.array([255, 0, 219, 0, 268, 0, 249, 0])
    model['B_birth'][4] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][4] = np.dot(
        model['B_birth'][4], model['B_birth'][4].T)

    # birth term 6
    model['w_birth'][5] = 3 / 100
    model['m_birth'][:, 5] = np.array([280, 0, 219, 0, 291, 0, 249, 0])
    model['B_birth'][5] = np.diag(np.array([10, 10, 10, 10, 10, 10, 10, 10]))
    model['P_birth'][5] = np.dot(
        model['B_birth'][5], model['B_birth'][5].T)

    # observation model parameters (noisy x/y only)

    # observation matrix
    h1 = np.array([1, 0, 0, 0, 0, 0, 0, 0])
    h2 = np.array([0, 0, 1, 0, 0, 0, 0, 0])
    h3 = np.array([0, 0, 0, 0, 1, 0, 0, 0])
    h4 = np.array([0, 0, 0, 0, 0, 0, 1, 0])

    model['H'] = np.array([h1, h2, h3, h4])

    model['D'] = np.diag(np.array([10, 10, 10, 10]))

    # observation noise covariance
    model['R'] = np.dot(model['D'], model['D'].T)

    # detection parameters

    # probability of detection in measurements
    model['P_D'] = 0.98

    # probability of missed detection in measurements
    model['Q_D'] = 1 - model['P_D']

    # clutter parameters

    # poisson average rate of uniform clutter (per scan)
    model['lambda_c'] = 0

    # uniform clutter region
    model['range_c'] = np.array(
        [[0, 640], [0, 480], [0, 640], [0, 480]])

    # uniform clutter density
    diff = np.array(
        (model['range_c'][:, 1] - model['range_c'][:, 0]), dtype=np.float64
    )

    model['pdf_c'] = 1 / np.prod(diff)

    print('Model components computed')
    print('')

    return model
