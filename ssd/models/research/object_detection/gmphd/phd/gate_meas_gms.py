import numpy as np


def gate_meas_gms(z=None, gamma=None, model=None, m=None, P=None):

    try:
        zlength = z.shape[1]
        valid_idx = [0]
        plength = m.shape[1]

        for j in range(plength):

            Sj = model['R'] + np.dot(np.dot(model['H'], P[j]), model['H'].T)
            Vs = np.linalg.cholesky(Sj)
            det_Sj = np.prod(np.diag(Vs)) ** 2
            inv_sqrt_Sj = np.linalg.inv(Vs)
            iSj = np.dot(inv_sqrt_Sj, inv_sqrt_Sj.T)

            nu = z - np.dot(
                model['H'], np.tile(
                    m[:, j].reshape(m[:, j].size, 1), (1, zlength))
            )

            def get_indexes(x, xs): return [i for (
                y, i) in zip(xs, range(len(xs))) if x > y]

            dist = np.sum(np.dot(inv_sqrt_Sj.T, nu) ** 2, axis=0)
            dist = dist.reshape(dist.size, 1)

            valid_idx = np.union1d(valid_idx, get_indexes(gamma, dist))
            valid_idx = [int(l) for l in valid_idx]

        z_gate = z[:, valid_idx]

        return z_gate

    except AttributeError as _:
        pass
