import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import pathlib
from gmphd.centroid.centroidtracker import CentroidTracker
from gmphd.utility.helper import location_speed, centroid, roi, speed
from scipy.stats import wasserstein_distance as wd
import os

def plot_results(dataset, model, truth, est=None):

    print('Ploting results')
    print('Please wait')

    plt.close()
    fig, ax = plt.subplots()
    ax.set_axis_off()
    ax.autoscale(True)
    ax.set_xlim([0, 640])
    ax.set_ylim([480, 0])

    img = None

    cl = ['b', 'c', 'm', 'r', 'y'] * int(truth['length'] / 5)
    est['estimate_boxes'] = np.array(est['estimate_boxes'])
    ct = CentroidTracker()

    track_roi = [[] for i in range(truth['length'])]
    track_ids = [[] for i in range(truth['length'])]

    wESTs = []
    wGTs = []

    false_dt = 0

    step_WD = []

    try:
        for i in range(truth['length']):
            step_ESTs = []
            for a in est['estimate_boxes'][i]:
                a = a[0]
                try:
                    ests = np.dot(model['H'], a)
                    for num in ests:
                        step_ESTs.append(num)
                        wESTs.append(num)
                except ValueError as _:
                    wESTs.append(0)
                    false_dt += 1
                    pass
            try:
                step_GTs = []
                gts = np.dot(model['H'], truth['groud_truth'][i])
                for nums in gts.T:
                    for l in nums:
                        step_GTs.append(l)
                        wGTs.append(l)

                step_WD.append(wd(step_GTs, step_ESTs))
            except ValueError as _:
                pass
    except IndexError as _:
        pass

    total_WD = wd(wGTs, wESTs)

    # print(round(total_WD, 2), 'WD')
    # print(int(len(wGTs)/2), 'truth length')
    # print(int(len(wESTs) / 2) + false_dt, 'estimate length')
    # print(step_WD)

    for i in range(truth['length']):

        try:
            temp_arr = np.zeros((len(est['estimate_boxes'][i]), len(est['estimate_boxes'][i][0][0])))

            for en, j in enumerate(est['estimate_boxes'][i]):
                j = j.squeeze()
                try:
                    temp_arr[en] = j
                except ValueError as _:
                    pass

            temp_arr = temp_arr[:, [0, 2, 4, 6]]
            objects = ct.update(temp_arr)

            oid = np.zeros((len(objects)), dtype=np.int64)
            cent = np.zeros((len(objects), 4), dtype=np.int64)

            for el, (objectID, objectCentroid) in enumerate(objects.items()):
                oid[el] = objectID
                cent[el] = objectCentroid

            track_ids[i] = oid
            track_roi[i] = cent
        except IndexError as _:
            pass

    for i in range(truth['length']):

        try:
            for j in est['estimate_boxes'][i]:
                j = j.squeeze()
                ys = np.zeros((model['z_dim']), dtype="int")
                ids = track_ids[i]
                tracks = track_roi[i]
                ys = j[[0, 2, 4, 6]].astype(int)

                for u, xs in enumerate(tracks):
                    if np.array_equal(xs, ys):
                        box, x, y = roi(ys, cl[ids[u]])
                        ax.add_patch(box)
                        ax.text(x, y, ids[u], bbox=dict(
                            boxstyle='round', facecolor=cl[ids[u]], alpha=0.5))
                        cent, vx, vy = speed(j)
                        # FPS = 14
                        # distance_x = vx / FPS
                        # distance_y = vy / FPS
                        if(vx != 0 or vy != 0):
                            my_prediction = [j[0], j[2], j[4], j[6]]
                            # print(my_prediction)
                        ax.quiver(cent[0], cent[1], vx, vy,
                                  color=cl[ids[u]], angles='xy', scale_units='xy')
            # print ("GMPHD Image: " + str(i))
            try:
                if img is None:
                    img = plt.imshow(truth['Images'][i])
                    # fig.show()

                else:
                    img.set_data(truth['Images'][i])
                    fig.canvas.draw()
                if i >= 0 and i < 10:
                    stri = "/home/fdai5607/thesis/datasets/images/{}/png/image_0000000{}_0.png'".format(dataset, i)
                    print (os.path.exists(stri))
                    fig.savefig('/home/fdai5607/thesis/datasets/images/{}/png/image_0000000{}_0.png'.format(dataset, i))
                elif i >= 10 and i < 100:
                    fig.savefig('/home/fdai5607/thesis/datasets/images/{}/png/image_000000{}_0.png'.format(dataset, i))
                elif i >= 100 and i < 1000:
                    fig.savefig('/home/fdai5607/thesis/datasets/images/{}/png/image_00000{}_0.png'.format(dataset, i))


                plt.pause(model['FT'])

                # Delete current patches and bounding boxes for next observation
                del ax.patches[:]
                del ax.collections[:]
                del ax.texts[:]

            except TypeError as _:
                pass

        except KeyboardInterrupt as _:
            break
