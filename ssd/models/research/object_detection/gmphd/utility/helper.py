import numpy as np
import matplotlib.patches as patches


def state_split(state):
    x = state[0]
    vx = state[1]
    y = state[2]
    vy = state[3]
    w = state[4]
    h = state[6]

    return x, vx, y, vy, w, h


def box(box):
    x = box[0]
    y = box[1]
    w = box[2]
    h = box[3]

    return x, y, w, h


def roi(box_, color):
    x, y, w, h = box(box_)

    w = w - x
    h = h - y

    rect = patches.Rectangle(
        (x, y), w, h,
        linewidth=2,
        edgecolor=color,
        facecolor='none'
    )

    return rect, x, y


def speed(state):
    _, vx, _, vy, _, _ = state_split(state)
    cent = centroid(state)

    return cent, vx, vy


def location_speed(state, color):
    '''
    Return patch, x, y, w, h, vx, vy
    '''
    x, vx, y, vy, w, h = state_split(state)
    cent = centroid(state)
    w = w - x
    h = h - y

    rect = patches.Rectangle(
        (x, y), w, h,
        linewidth=2,
        edgecolor=color,
        facecolor='none'
    )

    return rect, cent, vx, vy, x, y


def centroid(state):
    '''
    Return x and y centroid
    '''
    x, _, y, _, w, h = state_split(state)

    x = int((x + w)/2)
    y = int((y + h)/2)

    return np.array([x, y])


def extract_tracks(X, track_list, total_tracks):

    track_list = np.array(track_list)

    for i, _ in enumerate(track_list):
        track_list[i] = np.array(track_list[i])

    K = X.shape[0]
    x_dim = X[K-2].shape[0]
    k = K - 1

    while x_dim == 0:
        x_dim = X[k].shape[0]
        k = k - 2

    X_track = np.zeros((total_tracks, x_dim, K))
    k_birth = np.zeros((total_tracks, 1), dtype=int)
    k_death = np.zeros((total_tracks, 1), dtype=int)

    max_idx = 0

    for k in range(K):

        if X[k] != []:
            X_track[track_list[k], :, k] = X[k].T

            print(X_track[track_list[k], :, k])

        if np.max(track_list[k], axis=0) > max_idx:

            def get_indexes(x, xs): return [i for (
                y, i) in zip(xs, range(len(xs))) if x < y]

            idx = get_indexes(max_idx, track_list[k])
            k_birth[track_list[k][idx]] = k

        if track_list[k] != []:
            max_idx = np.max(track_list[k], axis=0)

        k_death[track_list[k]] = k

    return X_track, k_birth, k_death
