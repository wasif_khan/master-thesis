import numpy as np

class InverseMapping:

    def __init__(self):
        self.all_possible_aspect_ratios = np.array([1., 0.5, 1./3, 2, 3])
        self.aspect_ratio_3_boxes = [1, 0.5, 2]
        self.aspect_ratio_6_boxes = [1, 0.5, 2, 1./3, 3, 1]

    def compute_aspect_ratio_index(self, scale, aspect_ratio):
        if scale == 0:
            return self.aspect_ratio_3_boxes.index(aspect_ratio)
        else:
            return self.aspect_ratio_6_boxes.index(aspect_ratio)
            
    def calculateArea(self, box):
        return (box[2] - box[0]) * (box[3] - box[1])
    
    def compareAreas(self, areas, area):
        diff = 1000000000
        closest_scale = 0
        for i in range(len(areas)):
            if abs(areas[i] - area) < diff:
                diff = abs(areas[i] - area)
                closest_scale = i
        return closest_scale
    
    def compute_closest_scale(self, ar, all_boxes, my_box):
        for_3_boxes = 100
        try:
            for_3_boxes = self.aspect_ratio_3_boxes.index(ar)
        except:
            pass
        for_6_boxes = self.aspect_ratio_6_boxes.index(ar)
        boxes = { 'data': [ { 'scale': 1, 'box': all_boxes[1][5][5][for_6_boxes] }, { 'scale': 2, 'box': all_boxes[2][3][3][for_6_boxes] }, { 'scale': 3, 'box': all_boxes[3][2][2][for_6_boxes]}, { 'scale': 4, 'box': all_boxes[4][1][1][for_6_boxes]}, { 'scale': 5, 'box': all_boxes[5][0][0][for_6_boxes]} ] }
        if for_3_boxes != 100:
            boxes['data'].append({ 'scale': 0, 'box': all_boxes[0][10][10][for_3_boxes]})

        area_of_my_box = self.calculateArea(my_box)
        areas_of_anchor_boxes = []
        for box in boxes['data']:
            areas_of_anchor_boxes.append(self.calculateArea(box['box']))
        
        return boxes['data'][self.compareAreas(areas_of_anchor_boxes, area_of_my_box)]['scale']

    def bb_intersection_over_union(self, boxA, boxB):
        # determine the (x, y)-coordinates of the intersection rectangle
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])
    
        # compute the area of intersection rectangle
        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    
        # compute the area of both the prediction and ground-truth
        # rectangles
        boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    
        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = interArea / float(boxAArea + boxBArea - interArea)
    
        # return the intersection over union value
        return iou

    def calculate_eucledian_distance(self, my_box, bboxes, scale):
        min_values = []
        min_values_indexes = []
        distance_and_index = {}
        my_box_center = [0, 0]
        my_box_center[0] = round((my_box[2] + my_box[0])/2)
        my_box_center[1] = round((my_box[3] + my_box[1])/2)

        # for i in range(len(bboxes)):

        centers_of_bboxes = np.zeros([bboxes[scale].shape[0], bboxes[scale].shape[1], bboxes[scale].shape[2], 2])
        centers_of_bboxes[:, :, :, 0] = np.round((bboxes[scale][:, :, :, 2] +  bboxes[scale][:, :, :, 0]) / 2)
        centers_of_bboxes[:, :, :, 1] = np.round((bboxes[scale][:, :, :, 3] +  bboxes[scale][:, :, :, 1]) / 2)
        distances = np.zeros([bboxes[scale].shape[0], bboxes[scale].shape[1], bboxes[scale].shape[2]])

        distances = np.sqrt(np.square( centers_of_bboxes[:, :, :, 0] - my_box_center[0] ) + np.square( centers_of_bboxes[:, :, :, 1] - my_box_center[1] )) 

            # distances = np.sqrt(np.square(bboxes[i][:, :, :, 0] - my_box[0]) + np.square(bboxes[i][:, :, :, 1] - my_box[1]) + np.square(bboxes[i][:, :, :, 2] - my_box[2]) + np.square(bboxes[i][:, :, :, 3] - my_box[3]))
        # min_values.append(np.min(distances))
        # min_values_indexes.append(np.unravel_index(np.argmin(distances), distances.shape))

        distance_and_index['distance'] = round(np.argmin(distances), 2)
        # distance_and_index['distance'] = round(min_values[np.argmin(min_values)], 2)
        # distance_and_index['index'] = min_values_indexes[np.argmin(min_values)]
        distance_and_index['index'] = np.unravel_index(np.argmin(distances), distances.shape)
        # distance_and_index['scale'] = np.argmin(min_values)
        return distance_and_index

    def calculate_eucledian_distance_for_whole_box(self, my_box, bboxes):
        img_size = 300
        min_values = []
        min_values_indexes = []
        distance_and_index = {}
        my_box_center = [0, 0]
        my_box_center[0] = round((my_box[2] + my_box[0])/2)
        my_box_center[1] = round((my_box[3] + my_box[1])/2)

        for i in range(len(bboxes)):

            distances = np.zeros([bboxes[i].shape[0], bboxes[i].shape[1], bboxes[i].shape[2]])
            distances = np.sqrt(np.square(bboxes[i][:, :, :, 0] - my_box[0]) + np.square(bboxes[i][:, :, :, 1] - my_box[1]) + np.square(bboxes[i][:, :, :, 2] - my_box[2]) + np.square(bboxes[i][:, :, :, 3] - my_box[3]))
            min_values.append(np.min(distances))
            min_values_indexes.append(np.unravel_index(np.argmin(distances), distances.shape))

        distance_and_index['distance'] = round(min_values[np.argmin(min_values)], 2)
        distance_and_index['index'] = min_values_indexes[np.argmin(min_values)]
        distance_and_index['scale'] = np.argmin(min_values)
        return distance_and_index

    def calculate_aspect_ratio(self, my_box):
        h = my_box[2] - my_box[0]
        w = my_box[3] - my_box[1]
        return round(h/w, 2)

    def compute_closest_aspect_ratio(self, aspect_ratio_of_my_box):
        
        closest_ = np.sqrt(np.square(self.all_possible_aspect_ratios - aspect_ratio_of_my_box))
        return self.all_possible_aspect_ratios[np.argmin(closest_)]

    def compute_scale_of_box(self, all_anchor_boxes, my_box):

        jaccard_index_array = []
        for i in range(len(all_anchor_boxes)):
            jaccard_index_array.append(np.zeros([all_anchor_boxes[i].shape[0], all_anchor_boxes[i].shape[1], all_anchor_boxes[i].shape[2]]))

        for i in range(len(all_anchor_boxes)):
            for j in range(len(all_anchor_boxes[i])):
                for k in range(len(all_anchor_boxes[i][j])):
                    for l in range(len(all_anchor_boxes[i][j][k])):
                        jaccard_index_array[i][j][k][l] = self.bb_intersection_over_union(all_anchor_boxes[i][j][k][l], my_box)
        max_values = []
        max_values_index = []
        shape_and_index = {}
        for i in range(len(all_anchor_boxes)):
            # If you want to get the index of maximum value of jaccard index
            max_values_index.append(np.unravel_index(np.argmax(jaccard_index_array[i]), jaccard_index_array[i].shape))

            max_values.append(np.max(jaccard_index_array[i]))
        shape_and_index['shape'] = all_anchor_boxes[np.argmax(max_values)].shape
        shape_and_index['matrix_index'] = max_values_index[np.argmax(max_values)]
        shape_and_index['scale_index'] = np.argmax(max_values)

        return shape_and_index

    def checking_inverse_boxing(self, all_prediction_layers = [], scale = 0, indexes = (0,0), aspect_ratio = 0):

        for i in range(len(all_prediction_layers)):
            all_prediction_layers[i].fill(0)
        all_prediction_layers[scale][indexes[0], indexes[1], aspect_ratio, : ].fill(1)
        return all_prediction_layers

