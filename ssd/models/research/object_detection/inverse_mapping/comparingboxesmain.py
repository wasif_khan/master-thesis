import comparingboxes as cm
import numpy as np
import random

c = cm.comparing()

def calculate_scale_for_given_box(my_box, all_anchor_boxes):
    scale = c.compute_closest_scale(closest_ar, all_anchor_boxes, my_box)

for my_box in my_boxes:
# my_box = [71, 20, 296, 43]
    print('------------------------------------------')
    print('Given Box: ', my_box)
    ######################## Aspect Ratio #######################################
    ar = c.calculate_aspect_ratio(my_box)
    closest_ar = c.compute_closest_aspect_ratio(ar)
    print('my box Aspect Ratio: ', ar)
    print('closesest Anchor Box aspect ratio: ', closest_ar)

####################### Scale ###############################################

    scale1 = c.compute_scale_of_box(all_anchor_boxes, my_box)
    print('Scale from Jaccard Index:', scale1['scale_index'])
    scale = c.compute_closest_scale(closest_ar, all_anchor_boxes, my_box)
    print('Scale from Area: ', scale)
    did_not_match = False
    if(scale1['scale_index'] != scale):
        did_not_match = True
        print('Did not match!!!')

    ########################################### Eucledian Distance ########################
    min_distance = c.calculate_eucledian_distance(my_box, all_anchor_boxes, scale)
    print('Eucledian Distance: ', min_distance)
    min_distance_index = min_distance['index']

    min_distance_whole = c.calculate_eucledian_distance_for_whole_box(my_box, all_anchor_boxes)
    print('Eucledian Distance Whole: ', min_distance_whole)
    scale_by_distance = min_distance_whole['scale']

    index_aspect_ratio = c.compute_aspect_ratio_index(scale1['scale_index'], closest_ar)
    print('Index of Aspect Ratio: ', c.compute_aspect_ratio_index(scale, closest_ar))

    if(did_not_match):
        print('Closest Anchor Box By Jaccard Index Scale: ', np.round(all_anchor_boxes[scale1['scale_index']][min_distance_index[0]][min_distance_index[1]][index_aspect_ratio]))

    print('Closest Anchor Box: ', np.round(all_anchor_boxes[scale][min_distance_index[0]][min_distance_index[1]][index_aspect_ratio]))

    print('Closest Anchor Box By Eucledian Distance Whole: ', np.round(all_anchor_boxes[scale_by_distance][min_distance_whole['index'][0]][min_distance_whole['index'][1]][min_distance_whole['index'][2]]))
