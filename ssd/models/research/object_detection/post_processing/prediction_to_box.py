import numpy as np

class ComputeBoxFromPrediction:

    def extract_detections(self, all_layers_predictions, normalized = True, image_size = [640, 480], off_sets = []):
        normalized = True
        IMG_WIDTH = image_size[0] # 640
        IMG_HEIGHT = image_size[1] # 480

        # Inserting 4 zeroes in start so that detections can be appended
        detected_boxes = np.zeros([1,4], dtype=np.float32)

        # Anchor Boxes
        # all_anchor_boxes = np.load('boxes_all.npy', allow_pickle = True)
        all_anchor_boxes = self.offset_with_prior_scaling(off_sets)
        detected_boxes_index = []
        scale = 0

        all_anchor_boxes_with_image = []
        for i in range(len(all_layers_predictions)):
            indexes = np.where( all_layers_predictions[i][:,:,:,1] > 0)

            if normalized == False:

                detections_ymin = all_anchor_boxes [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[0]] * IMG_HEIGHT
                detections_xmin = all_anchor_boxes [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[1]] * IMG_WIDTH
                detections_ymax = all_anchor_boxes [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[2]] * IMG_HEIGHT
                detections_xmax = all_anchor_boxes [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[3]] * IMG_WIDTH
                detection = np.stack((detections_ymin, detections_xmin, detections_ymax, detections_xmax), axis=1)
                detection = np.round(np.squeeze(detection,axis=2))

                ################################# Offsets Normalization ##########################

                # off_sets_detections_ymin = off_sets [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[0]]
                # off_sets_detections_xmin = off_sets [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[1]]
                # off_sets_detections_ymax = off_sets [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[2]]
                # off_sets_detections_xmax = off_sets [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ][:,[3]]
                # off_sets_detection = np.stack((off_sets_detections_ymin, off_sets_detections_xmin, off_sets_detections_ymax, off_sets_detections_xmax), axis=1)
                # off_sets_detection = np.squeeze(off_sets_detection,axis=2)

                # detection += off_sets [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ]
                # detection += off_sets_detection
                detection = np.round(detection)
                
            else:
                detection = all_anchor_boxes [scale] [indexes [0] [:], indexes [1] [:], indexes [2] [:] ]

            detected_boxes = np.append(detected_boxes, detection, axis=0)

            scale += 1

        #Return from row 1 because we appened 0 row just for the purpose of making it appendable
        return detected_boxes[1:,:]

    def calculate_centers(self, img_height, image_width):
        all_anchor_boxes = np.load('boxes_all.npy', allow_pickle = True)
        centers_of_boxes = []
        for scale_box in all_anchor_boxes:
            centerh = ((scale_box[:,:,:,0] + scale_box[:,:,:,2])/ 2 * img_height).astype(int)
            centerw = ((scale_box[:,:,:,1] + scale_box[:,:,:,3])/ 2 * image_width).astype(int)
            centers = np.stack((centerh, centerw), axis=3)
            centers = np.round(centers)
            centers_of_boxes.append(centers)
        
        return centers_of_boxes

    def convert_anchor_boxes_to_cx_cy_w_h(self, img_height = 300, image_width = 300):
        all_anchor_boxes = np.load('boxes_all.npy', allow_pickle = True)
        anchor_boxes = []
        for scale_box in all_anchor_boxes:
            center_cy = ((scale_box[:,:,:,0] + scale_box[:,:,:,2])/ 2 * img_height).astype(int)
            center_cx = ((scale_box[:,:,:,1] + scale_box[:,:,:,3])/ 2 * image_width).astype(int)
            h = (np.abs(scale_box[:,:,:,0] - scale_box[:,:,:,2]) * img_height).astype(int)
            w = (np.abs(scale_box[:,:,:,1] - scale_box[:,:,:,3]) * image_width).astype(int)
            anchor_box = np.stack((center_cy, center_cx, h, w), axis=3)
            anchor_box = np.round(anchor_box)
            anchor_boxes.append(anchor_box)
        
        return anchor_boxes
    
    def offset_with_prior_scaling(self, offset, prior_scaling=[0.1, 0.1, 0.2, 0.2]):
        center_anchor_boxes = self.convert_anchor_boxes_to_cx_cy_w_h(300, 300)
        new_bboxes = []
        for i in range(len(offset)):

            yref = center_anchor_boxes[i][:,:,:,0]
            xref = center_anchor_boxes[i][:,:,:,1]
            href = center_anchor_boxes[i][:,:,:,2]
            wref = center_anchor_boxes[i][:,:,:,3]
            cy = offset[i][:, :, :, 0] * href * prior_scaling[0] + yref
            cx = offset[i][:, :, :, 1] * wref * prior_scaling[1] + xref
            h = href * np.exp(offset[i][:, :, :, 2] * prior_scaling[2])
            w = wref * np.exp(offset[i][:, :, :, 3] * prior_scaling[3])
            bboxes = np.zeros_like(offset[i])
            bboxes[:, :, :, 0] = cy - h / 2.
            bboxes[:, :, :, 1] = cx - w / 2.
            bboxes[:, :, :, 2] = cy + h / 2.
            bboxes[:, :, :, 3] = cx + w / 2.
            new_bboxes.append(bboxes)
        return new_bboxes
