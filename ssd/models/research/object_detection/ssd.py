import matplotlib
matplotlib.use('Agg')
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import scipy.misc

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from object_detection.gmphd import demo

from object_detection.post_processing import prediction_to_box
from object_detection.inverse_mapping import comparingboxes

if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
  raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')
# This is needed to display the images.
# %matplotlib inline
from utils import label_map_util

from utils import visualization_utils as vis_util

def get_anchor_boxes():

  all_anchor_boxes_normalized = np.load('boxes_all.npy', allow_pickle = True)
  IMG_HEIGHT = 480
  IMG_WIDTH = 640
  all_anchor_boxes = []

  for i in range(all_anchor_boxes_normalized.shape[0]):

    boxes_ymin = all_anchor_boxes_normalized[i][:,:,:,0] * IMG_HEIGHT
    boxes_xmin = all_anchor_boxes_normalized[i][:,:,:,1] * IMG_WIDTH
    boxes_ymax = all_anchor_boxes_normalized[i][:,:,:,2] * IMG_HEIGHT
    boxes_xmax = all_anchor_boxes_normalized[i][:,:,:,3] * IMG_WIDTH
    all_anchor_boxes.append(np.stack((boxes_ymin, boxes_xmin, boxes_ymax, boxes_xmax), axis=3))
  return all_anchor_boxes

def checking_inverse_mapping(all_prediction_layers, scale, indexes, aspect_ratio):
  c = comparingboxes.InverseMapping()
  all_prediction_layers = c.checking_inverse_boxing(all_prediction_layers, scale, indexes, aspect_ratio)
  return all_prediction_layers

def inverse_mapping_input(inverse_mapped_boxes_list):
  c = comparingboxes.InverseMapping()
  all_anchor_boxes = get_anchor_boxes()

  scales = []
  indexes = []
  for my_box in inverse_mapped_boxes_list:
    ar = c.calculate_aspect_ratio(my_box)
    closest_ar = c.compute_closest_aspect_ratio(ar)
    scale = c.compute_closest_scale(closest_ar, all_anchor_boxes, my_box)

    min_distance = c.calculate_eucledian_distance(my_box, all_anchor_boxes, scale)
    min_distance_index = min_distance['index']
    
    scales.append(scale)
    indexes.append(min_distance_index)

  return scales, indexes
  
# What model to download.
MODEL_NAME = 'ssd_mobilenet_v2_coco_2018_03_29'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  image_np = np.array(image.getdata())[:,:3]
  return image_np.reshape((im_height, im_width, 3)).astype(np.uint8)

def load_masked_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 1)).astype(np.uint8)

def load_masked_image_paths(base_path = '/home/fdai5607/thesis/datasets/images/bahnhof/masks/', path_to_file = '/home/fdai5607/thesis/datasets/images/bahnhof/masks/masked_images.txt'):
  
  masked_image_paths = []
  mask_directory = open(path_to_file, "r")
  for i in mask_directory:
    masked_image_paths.append(base_path + '/' + i.rstrip())
  return masked_image_paths



def load_image_paths(main_loop_iterators_to_test, start_from, base_path = '/home/fdai5607/thesis/datasets/images/bahnhof/left/'):
  PATH_TO_TEST_IMAGES_DIR = base_path
  TEST_IMAGE_PATHS = np.array([])

  # main_loop_iterators_to_test = 3
  # start_from = 10
  for i in range(start_from + main_loop_iterators_to_test)[start_from: ]:
    if(i<10):
      TEST_IMAGE_PATHS = np.append(TEST_IMAGE_PATHS, os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image_0000000{}_0.png'.format(i)))
    elif(i>=10 and i<100):
      TEST_IMAGE_PATHS = np.append(TEST_IMAGE_PATHS, os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image_000000{}_0.png'.format(i)))
        
    elif(i>= 100 and i<1000):
      TEST_IMAGE_PATHS = np.append(TEST_IMAGE_PATHS, os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image_00000{}_0.png'.format(i)))
  return TEST_IMAGE_PATHS

# Size, in inches, of the output images.
IMAGE_SIZE = (12, 8)

def get_offsets(output_dict):
  offset_boxes1 = output_dict['BoxPredictor_0/Reshape']
  offset_boxes2 = output_dict['BoxPredictor_1/Reshape']
  offset_boxes3 = output_dict['BoxPredictor_2/Reshape']
  offset_boxes4 = output_dict['BoxPredictor_3/Reshape']
  offset_boxes5 = output_dict['BoxPredictor_4/Reshape']
  offset_boxes6 = output_dict['BoxPredictor_5/Reshape']

  offset_boxes1 = offset_boxes1.reshape([19, 19, 3, 4])
  offset_boxes2 = offset_boxes2.reshape([10, 10, 6, 4])
  offset_boxes3 = offset_boxes3.reshape([5, 5, 6, 4])
  offset_boxes4 = offset_boxes4.reshape([3, 3, 6, 4])
  offset_boxes5 = offset_boxes5.reshape([2, 2, 6, 4])
  offset_boxes6 = offset_boxes6.reshape([1, 1, 6, 4])

  all_offsets_layers = [offset_boxes1, offset_boxes2, offset_boxes3, offset_boxes4, offset_boxes5, offset_boxes6]

def get_predictions(output_dict):
  prediction_boxes1 = output_dict['BoxPredictor_0/Reshape_1']
  prediction_boxes2 = output_dict['BoxPredictor_1/Reshape_1']
  prediction_boxes3 = output_dict['BoxPredictor_2/Reshape_1']
  prediction_boxes4 = output_dict['BoxPredictor_3/Reshape_1']
  prediction_boxes5 = output_dict['BoxPredictor_4/Reshape_1']
  prediction_boxes6 = output_dict['BoxPredictor_5/Reshape_1']

  prediction_boxes1 = prediction_boxes1.reshape([19, 19, 3, 91])
  prediction_boxes2 = prediction_boxes2.reshape([10, 10, 6, 91])
  prediction_boxes3 = prediction_boxes3.reshape([5, 5, 6, 91])
  prediction_boxes4 = prediction_boxes4.reshape([3, 3, 6, 91])
  prediction_boxes5 = prediction_boxes5.reshape([2, 2, 6, 91])
  prediction_boxes6 = prediction_boxes6.reshape([1, 1, 6, 91])
  
  all_prediction_layers = [prediction_boxes1, prediction_boxes2, prediction_boxes3, prediction_boxes4, prediction_boxes5, prediction_boxes6]

  return all_prediction_layers

def get_predictions_offsets(output_dict):
  prediction_boxes1 = output_dict['BoxPredictor_0/Reshape']
  prediction_boxes2 = output_dict['BoxPredictor_1/Reshape']
  prediction_boxes3 = output_dict['BoxPredictor_2/Reshape']
  prediction_boxes4 = output_dict['BoxPredictor_3/Reshape']
  prediction_boxes5 = output_dict['BoxPredictor_4/Reshape']
  prediction_boxes6 = output_dict['BoxPredictor_5/Reshape']

  prediction_boxes1 = prediction_boxes1.reshape([19, 19, 3, 4])
  prediction_boxes2 = prediction_boxes2.reshape([10, 10, 6, 4])
  prediction_boxes3 = prediction_boxes3.reshape([5, 5, 6, 4])
  prediction_boxes4 = prediction_boxes4.reshape([3, 3, 6, 4])
  prediction_boxes5 = prediction_boxes5.reshape([2, 2, 6, 4])
  prediction_boxes6 = prediction_boxes6.reshape([1, 1, 6, 4])
  
  all_prediction_layers = [prediction_boxes1, prediction_boxes2, prediction_boxes3, prediction_boxes4, prediction_boxes5, prediction_boxes6]

  return all_prediction_layers

def run_inference_for_single_image(image, graph, boxes_inside_mask_indexes = [], delta = 1):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      k = 0
      tensor_dict = {}
      for key in [
        'BoxPredictor_0/Reshape',
        'BoxPredictor_1/Reshape',
        'BoxPredictor_2/Reshape',
        'BoxPredictor_3/Reshape',
        'BoxPredictor_4/Reshape',
        'BoxPredictor_5/Reshape',
        'BoxPredictor_0/Reshape_1',
        'BoxPredictor_1/Reshape_1',
        'BoxPredictor_2/Reshape_1',
        'BoxPredictor_3/Reshape_1',
        'BoxPredictor_4/Reshape_1',
        'BoxPredictor_5/Reshape_1',
        'BoxPredictor_0/Reshape',
        'BoxPredictor_1/Reshape',
        'BoxPredictor_2/Reshape',
        'BoxPredictor_3/Reshape',
        'BoxPredictor_4/Reshape',
        'BoxPredictor_5/Reshape',
        'num_detections', 
        'detection_boxes', 
        'detection_scores',
        'detection_classes', 
        'detection_masks'
      ]:

        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)

      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[1], image.shape[2])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict, feed_dict={image_tensor: image})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.int64)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]

      all_prediction_layers = get_predictions(output_dict)

      ## Increasing scores for prediction boxes inside mask by delta
      if boxes_inside_mask_indexes != []:
        for j in boxes_inside_mask_indexes:
          all_prediction_layers[ j[0] ][ j[1], j[2] , j[3] , 1] = all_prediction_layers[ j[0] ][ j[1] , j[2] , j[3], 1 ] + delta
          
      all_prediction_layers_offset = get_predictions_offsets(output_dict)

      # Setting all values for all_prediction_layers to 0, just 1 cell value to 1 for checking 

      # scale_ = 3
      # indexes_ = (1,1) # First aurgument along height, 2nd is width
      # a_r = 1
      # all_prediction_layers = checking_inverse_mapping(all_prediction_layers, scale_, indexes_, a_r)
      # all_anchor_boxes = np.load('boxes_all.npy', allow_pickle = True)
      # this_box = [all_anchor_boxes[scale_][indexes_[0], indexes_[1], a_r][0] * 480, all_anchor_boxes[scale_][indexes_[0], indexes_[1], a_r][1] * 640, all_anchor_boxes[scale_][indexes_[0], indexes_[1], a_r][2] * 480, all_anchor_boxes[scale_][indexes_[0], indexes_[1], a_r][3] * 640]
      # print(np.round(this_box))


      # inversed_scales, inversed_indexes = inverse_mapping_input([[50,100,130,200], [50,100,400,500]])

      # for i in range(len(inversed_scales)):
      #   all_prediction_layers[inversed_scales[i]][inversed_indexes[i]] += 20 

      # image_size = [width, height]
      extractor = prediction_to_box.ComputeBoxFromPrediction()
      object_detections_from_fm = extractor.extract_detections(all_prediction_layers, normalized = False, image_size = [image.shape[2], image.shape[1]], off_sets = all_prediction_layers_offset)

  return output_dict, object_detections_from_fm

def get_boxes_from_detection_and_write(output_dict, main_loop_iterator, w, h, annotation_file, normalized = True, no_of_detection = 0, annotation_starting_name = "image"):

  if normalized != True:
    w = 1
    h = 1
  any_detection = False
  is_first = True
  string_data = ""
  for i in range(no_of_detection):
    if(output_dict["detection_classes"][i]==1 and output_dict["detection_scores"][i] != 0):
      
      any_detection = True
      box = tuple(output_dict["detection_boxes"][i].tolist())

      if(is_first):
        if(main_loop_iterator < 10):
          string_data += annotation_starting_name + str(main_loop_iterator) + "_0.png\": "
        elif(main_loop_iterator >= 10 and main_loop_iterator < 100):
          string_data += annotation_starting_name + str(main_loop_iterator) + "_0.png\": "
        elif(main_loop_iterator >= 100 and main_loop_iterator < 1000):
          string_data += annotation_starting_name + str(main_loop_iterator) + "_0.png\": "

        # main_loop_iterator = main_loop_iterator + 1
        is_first = False
      ymin, xmin, ymax, xmax = box

      # needed by GMPHD
      #  string_data += "(" + str(round(xmin*w)) + ", " + str(round(ymin*h)) + ", " + str(round(xmax*w)) + ", " + str(round(ymax*h))  + "), "

      # For comparsion with SSD results

      
      string_data += "(" + str(round(ymin*h)) + ", " + str(round(xmin*w)) + ", " + str(round(ymax*h)) + ", " + str(round(xmax*w))  + "), "

  if(any_detection):
    string_data = string_data[:-2]
    string_data += "\n"
  annotation_file.write(string_data)

i = 0
main_loop_iterator = 0
k = 0

y = 0

def find_all_prediction_boxes_inside_mask():

  extractor = prediction_to_box.ComputeBoxFromPrediction()

  ## In my case Image size if 480(h), 640(w)
  centers_of_boxes = extractor.calculate_centers(300, 300)
  masked_image_paths = load_masked_image_paths()
  images_paths = load_masked_image_paths(base_path='/home/fdai5607/thesis/datasets/images/bahnhof/left/')
  per_images_boxes_inside_mask = []
  for i in range(len(masked_image_paths)):
    image = Image.open(masked_image_paths[i]).convert('RGB')
    image = image.resize((300, 300))
    image = image.convert('1')
    image_np = load_masked_image_into_numpy_array(image)


    boxes_inside_mask = []
    #################################### Checking all prediction boxes that do they exist in mask or not #############################

    ################### This loop is for iterating over all scales of prediction boxes ###################
    for j in range(len(centers_of_boxes)): 

      ##################### Checking != 0 as either center of box is incide mask or not ###################
      indexes = np.where(image_np[centers_of_boxes[j][:,:,:,0],centers_of_boxes[j][:,:,:,1]] != 0)

      ############ This loop is for iterating for all matched indexes inside Mask, it is giving in for of 3 * any_number, here any_number are boxes which are inside mask ############################## 
      for k in range(indexes[0].shape[0]):

        # j represents scale for prediction boxes
        boxes_inside_mask.append((j, indexes[0][k], indexes[1][k], indexes[2][k]))
      
    per_images_boxes_inside_mask.append(boxes_inside_mask)
  
  return per_images_boxes_inside_mask 

# def increase_value_by_delta_for_boxes_inside_mask(prediction_boxes_indexes_list, detection_scores):



def non_max_suppression_fast(boxes, overlapThresh):
	# if there are no boxes, return an empty list
	if len(boxes) == 0:
		return []
 
	# if the bounding boxes integers, convert them to floats --
	# this is important since we'll be doing a bunch of divisions
	if boxes.dtype.kind == "i":
		boxes = boxes.astype("float")
 
	# initialize the list of picked indexes	
	pick = []
 
	# grab the coordinates of the bounding boxes
	x1 = boxes[:,0]
	y1 = boxes[:,1]
	x2 = boxes[:,2]
	y2 = boxes[:,3]
 
	# compute the area of the bounding boxes and sort the bounding
	# boxes by the bottom-right y-coordinate of the bounding box
	area = (x2 - x1 + 1) * (y2 - y1 + 1)
	idxs = np.argsort(y2)
 
	# keep looping while some indexes still remain in the indexes
	# list
	while len(idxs) > 0:
		# grab the last index in the indexes list and add the
		# index value to the list of picked indexes
		last = len(idxs) - 1
		i = idxs[last]
		pick.append(i)
 
		# find the largest (x, y) coordinates for the start of
		# the bounding box and the smallest (x, y) coordinates
		# for the end of the bounding box
		xx1 = np.maximum(x1[i], x1[idxs[:last]])
		yy1 = np.maximum(y1[i], y1[idxs[:last]])
		xx2 = np.minimum(x2[i], x2[idxs[:last]])
		yy2 = np.minimum(y2[i], y2[idxs[:last]])
 
		# compute the width and height of the bounding box
		w = np.maximum(0, xx2 - xx1 + 1)
		h = np.maximum(0, yy2 - yy1 + 1)
 
		# compute the ratio of overlap
		overlap = (w * h) / area[idxs[:last]]
 
		# delete all indexes from the index list that have
		idxs = np.delete(idxs, np.concatenate(([last],
			np.where(overlap > overlapThresh)[0])))
 
	# return only the bounding boxes that were picked using the
	# integer data type
	return boxes[pick].astype("int")

def main_function(delta = 0):
  i = 0
  main_loop_iterator = 0
  k = 0

  y = 0

  boxes_inside_mask_indexes = find_all_prediction_boxes_inside_mask()
  # boxes_inside_mask_indexes = []
  # images_paths = load_image_paths(main_loop_iterators_to_test=3, start_from=0)
  images_paths = load_masked_image_paths(base_path='/home/fdai5607/thesis/datasets/images/bahnhof/left/')
  # images_paths = load_masked_image_paths(base_path='/home/fdai5607/thesis/ssd/models/research/object_detection/')
  annotation_file = open("/home/fdai5607/thesis/datasets/annotation/for-checking-bahnhof-annot.idl","w+")
  for image_path in images_paths:
    image = Image.open(image_path)
    image = image.resize((300, 300))
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    image_np1 = load_image_into_numpy_array(image)

    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)

    # Actual detection from ssd and my own post processing module
    output_dict, object_detections_from_fm = run_inference_for_single_image(image_np_expanded, detection_graph, boxes_inside_mask_indexes[k], delta)

    # output_dict, object_detections_from_fm = run_inference_for_single_image(image_np_expanded, detection_graph)

    # Apply non-maximum suppression
    non_max = non_max_suppression_fast(object_detections_from_fm, 0.5)

    my_detection_classes = np.array(np.ones(non_max.shape[0]), dtype=int)
    my_detection_scores = np.ones(non_max.shape[0])
    
    my_own_post_processing_output_dict = {} 
    my_own_post_processing_output_dict['detection_boxes'] = non_max
    my_own_post_processing_output_dict['detection_classes'] = my_detection_classes
    my_own_post_processing_output_dict['detection_scores'] = my_detection_scores

    # image = Image.fromarray(np.uint8(image)).convert('RGB')
    w, h = image.size
    print("Image No: " + str(k))
    k = k + 1

    annotation_starting_string = "\"left/image_0000000"
    my_annotation_starting_string = "\"my_left/image_0000000"

    get_boxes_from_detection_and_write(output_dict, main_loop_iterator, w, h, annotation_file, normalized = True, no_of_detection = len(output_dict['detection_boxes']), annotation_starting_name = annotation_starting_string)
    
    get_boxes_from_detection_and_write(my_own_post_processing_output_dict, main_loop_iterator, w, h, annotation_file, normalized = False, no_of_detection = my_own_post_processing_output_dict['detection_boxes'].shape[0], annotation_starting_name = my_annotation_starting_string)
    main_loop_iterator += 1

    vis_util.visualize_boxes_and_labels_on_image_array(image_np, output_dict['detection_boxes'], output_dict['detection_classes'], output_dict['detection_scores'], category_index, instance_masks=output_dict.get('detection_masks'), use_normalized_coordinates=True, line_thickness=2)

    # my_detection_classes = np.array(np.ones(object_detections_from_fm.shape[0]), dtype=int)
    # my_detection_scores = np.ones(object_detections_from_fm.shape[0])

    vis_util.visualize_boxes_and_labels_on_image_array(image_np1, my_own_post_processing_output_dict['detection_boxes'], my_own_post_processing_output_dict['detection_classes'], my_own_post_processing_output_dict['detection_scores'], category_index, instance_masks=output_dict.get('detection_masks'), use_normalized_coordinates=False, line_thickness=2)

    # plt.figure(figsize=IMAGE_SIZE)
    # plt.imshow(image_np)
    name= 'image_{}.png'.format(str(image_path).split('_')[1])
    Image.fromarray(image_np).save(name)
    # plt.savefig(name)
    
    name= './delta/delta_{}_image_my_ssd_{}.png'.format(str(delta), str(image_path).split('_')[1])
    Image.fromarray(image_np1).save(name)


    i = i+1
  annotation_file.close()

# for i in np.round(np.arange(1,4.5,0.5), decimals=1):
main_function(delta=1)
# find_all_prediction_boxes_inside_mask()